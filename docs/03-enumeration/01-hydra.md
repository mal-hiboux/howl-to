---
layout: default
title: Hydra
parent: Enumeration
nav_order: 10
---

# Hydra

```bash
hydra <Username options> <Password options> <Options> <IP Address> <Protocol> -V -f
```

## Supported Services

`adam6500 asterisk cisco cisco-enable cvs firebird ftp ftps http[s]-{head|get|post} http[s]-{get|post}-form http-proxy http-proxy-urlenum icq imap[s] irc ldap2[s] ldap3[-{cram|digest}md5][s] mssql mysql nntp oracle-listener oracle-sid pcanywhere pcnfs pop3[s] postgres radmin2 rdp redis rexec rlogin rpcap rsh rtsp s7-300 sip smb smtp[s] smtp-enum snmp socks5 ssh sshkey svn teamspeak telnet[s] vmauthd vnc xmpp
`

## Options
```
-l Single Username
-L Username list
-p Password
-P Password list
-t Limit concurrent connections
-V Verbose output
-f Stop on correct login
-s Port
```

## SSH
```
hydra -l root -P passwords.txt ssh://192.168.2.66 -V
```
## FTP
```
hydra -L usernames.txt -P passwords.txt 192.168.2.62 ftp -V -f
```

## SMB
```
hydra -l root -P passwords.txt smb://192.168.2.66  -V -f
```

## MySQL
```
hydra -L usernames.txt -P passwords.txt 192.168.2.66 mysql -V -f
```

Note: MySQL did not have a password set. I had to add a blank line in the password list.

## VNC
```
hydra -P passwords.txt vnc://192.168.2.62 -V
```

Note: VNC does not utilize a username and is not included in the command.

## Postgresql
```
hydra -L usernames.txt -P passwords.txt 192.168.2.62 postgres -V
```

## Telnet
```
hydra -L usernames.txt -P passwords.txt 192.168.2.62 telnet -V
```