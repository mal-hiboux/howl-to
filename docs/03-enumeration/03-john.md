---
layout: default
title: John The Ripper
parent: Enumeration
nav_order: 30
---

# John the ripper


## Show result

```
john --show
```

## Use other wordlist

```bash
john --wordlist=password.lst hashfile
```

## Convert to john

### Linux passwords

```
unshadow /etc/passwd /etc/shadow > users.txt
john users.txt
```

### rar2john

rar2john utility for RAR 3.x files, rar2john processes input RAR files into a format suitable for use with JtR.

```
rar2john [rar files]
john [output file]
```

### dmg2john

```
dmg2john
```

### gpg2john

```
gpg2john mykey.asc > hash
john --wordlist=pass.txt hash
```
### hccap2john

hccap2john processes input hccap files into a format suitable for use with JtR

```
hccap2john ./my.hccap > crackme
john --wordlist=rockyou-10.txt --format=wpapsk crackme
```
### keepass2john

Keepass2john processes input KeePass 1.x and 2.x * database files into a format suitable for use with JtR

```
keepass2john CrackThis.kdb > CrackThis.hash
```

### keychain2john

keepchain2john processes input Mac OS X keychain files into a format suitable for use with JtR.

```
keychain2john [keychain files]
```

### keyring2john

Cracking GNOME Keyring files

```
keyring2john Default.keyring > hash
john hash
```

### keystore2john

```
keystore2john [keystore file]
```

### kwallet2john

```
kwallet2john [kwallet file]
```

### luks2john

```
luks2john [file]
```

### pfx2john
```
pfx2john [file]
```

### putty2john
```
putty2john [file]
```

### pwsafe2john
```
pwsafe2john [file]
```

### racf2john
```
racf2john [file]
```

### rar2john
```
rar2john [file]
```

### ssh2john
```
ssh2john
```

### truecrypt_volume2john
```
truecrypt_volume2john [file]
```

### uaf2john
```
uaf2john  [file]
```

### wpapcap2john
```
wpapcap2john [file]
```

### zip2john

zip2john processes input ZIP files into a format suitable for use with JtR.

```
zip2john encrypted.zip > encrypted.hash
john --show encrypted.hash
```