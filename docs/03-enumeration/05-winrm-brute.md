---
layout: default
title: WinRM
parent: Enumeration
nav_order: 40
---
## winrm-brute

A brute-force tool against WinRM service.
Get winrm-brute from [GitHub](https://github.com/mchoji/winrm-brute)

```bash
$bundle exec ./winrm-brute.rb -U users.txt -P passwords.txt 10.0.0.1
```
