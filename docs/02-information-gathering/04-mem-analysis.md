---
layout: default
title: Memory Analysis
parent: Information Gathering
nav_order: 40
---

# Volatility
## Image identification: 

```bash
volatility -f image_name.dmp imageinfo
```
## Retrieve the hostname of the machine

### Dump the hives

Although the Windows registry appears as a single hierarchy in tools such as regedit, it is actually made
up of a number of different binary files called hives on disk. 

```bash
volatility -f image_name.dmp  --profile=Win7SP1x86_23418 hivelist
```
### Dump the registry key where the hostname will be revealed
```bash
volatility -f image_name.dmp  --profile=Win7SP1x86_23418  printkey -o 0x8b21c008  -K 'ControlSet001\Control\ComputerName\ComputerName'
```
## Password hashes

### Dump the hives (see  [Dump the hives](#dump the hives))

### Dump the password hashes

```bash
volatility -f image_name.dmp  --profile=Win7SP1x86_23418   hashdump -y 0x8b21c008 -s 0x9aad6148  > hashes.txt
```

## System processes

```bash
volatility -f image_name.dmp  --profile=Win7SP1x86_23418  pslist
```

## View the process listing in tree form 
```bash
volatility -f image_name.dmp  --profile=Win7SP1x86_23418  pstree
```

## Inactive or hidden processes
```bash
volatility -f image_name.dmp  --profile=Win7SP1x86_23418  passcan
```

## Commands on cmd
```bash
volatility -f image_name.dmp  --profile=Win7SP1x86_23418 cmdscan
```

## Scan for network artifacts:

```bash
volatility  -f image_name.dmp  --profile=Win7SP1x86_23418 netscan
```

## Display process command-line arguments
```bash
volatility  -f image_name.dmp  --profile=Win7SP1x86_23418 cmdline
```
## Scanning with yara

### Get yara rules for malware: 

```bash
wget https://gist.githubusercontent.com/andreafortuna/29c6ea48adf3d45a979a78763cdc7ce9/raw/4ec711d37f1b428b63bed1f786b26a0654aa2f31/malware_yara_rules.py
mkdir rules
python malware_yara_rules.py
```
### Scan:

```bash
volatility --profile=Win7SP1x86_23418 yarascan -y malware_rules.yar -f image_name.dmp
```
