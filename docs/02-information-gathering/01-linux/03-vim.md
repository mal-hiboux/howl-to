---
layout: default
title: Vim
parent: Linux
nav_order: 30
---

## The swap file

Vim stores the things you changed in a swap file.  Using the original file
you started from plus the swap file you can mostly recover your work.

You can see the name of the current swap file being used with the command:


	:sw[apname]					*:sw* *:swapname*
	

## Recovery :

You can recover a file's original data from its swap file using vim's recovery option :

```bash
vim -r file.swp
```