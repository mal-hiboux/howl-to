---
layout: default
title: Software Dependances
parent: Information Gathering
nav_order: 10
---

# Ruby

## Analyze gemfile.lock to file vulnerabilities
* https://audit.fastruby.io/
* https://hakiri.io/facets

# Node

```bash
npm audit
```