---
layout: default
title: WinRM
parent: Persistence
nav_order: 40
---

# WinRM for Ruby

## Install winrm using `gem`: 

```bash
gem install -r winrm
```

## WinRm connection: 

```ruby
require 'winrm'

conn = WinRM::Connection.new(
  endpoint: 'http://remote_ip:5985/wsman',
  user: 'domain_name\user_name',
  password: 'password',
)

command=""

conn.shell(:powershell) do |shell|
    until command == "exit\n" do
        print "PS > "
        command = gets
        output = shell.run(command) do |stdout, stderr|
            STDOUT.print stdout
            STDERR.print stderr
        end
    end
    puts "Exiting with code #{output.exitcode}"
end
```

More on [GitHub](https://github.com/WinRb/WinRM)